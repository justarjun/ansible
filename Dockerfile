FROM 1and1internet/ubuntu-16:latest
MAINTAINER krarjun@gmail.com

RUN apt-get update && apt-get install -y openssh-server openssh-client net-tools
RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

EXPOSE 22
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
